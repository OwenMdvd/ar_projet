﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cle : MonoBehaviour
{
    public bool tour;

    public bool open;

    public GameObject itsATrap;

    public GameObject rouage;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (!open)
        {
            transform.LookAt(target);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 90, -90);
        }*/
        
        if(tour && transform.eulerAngles.y >= 355)
        {
            Debug.Log(transform.eulerAngles.y);
            open = true;
            if (!itsATrap.GetComponent<Animator>().GetBool("Open"))
            {
                transform.parent = itsATrap.transform.GetChild(0);
                rouage.GetComponent<RouageChiffre>().canTurn = true;
                rouage.transform.GetChild(1).GetComponent<Animator>().SetBool("open", true);
                GameObject.FindGameObjectWithTag("grille").GetComponent<SonGrille>().PlaySon();
                Destroy(rouage.transform.GetChild(1).gameObject, 1.5f);
                itsATrap.GetComponent<Animator>().SetBool("Open", true);

                rouage.GetComponent<InteractGlow>().canGlow = true;

                transform.GetChild(0).GetComponent<InteractGlow>().canGlow = false;
                transform.GetChild(1).GetComponent<InteractGlow>().canGlow = false;
            }
        }
    }
}
