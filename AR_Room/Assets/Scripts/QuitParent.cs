﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class QuitParent : MonoBehaviour
{
    public void ParentNull()
    {
        GetComponent<Animator>().enabled = false;

        StartCoroutine(Null());

        //Vector3 pos = transform.position;
        //Vector3 rot = transform.localEulerAngles;
        //transform.parent = null;
        //transform.position = pos;
        //transform.eulerAngles = rot;
    }

    IEnumerator Null()
    {
        yield return new WaitForEndOfFrame();
        transform.parent = null;
    }
}
