﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Code : MonoBehaviour
{

    public enum codeType { bouton , keypad , manager, keypart, key };

    public codeType code;

    [DrawIf("code", codeType.bouton)] public Animator mecanism;
    [DrawIf("code", codeType.bouton)] public InteractGlow keyPartGlow;
    [DrawIf("code", codeType.bouton)] public bool pushOnce;
    public ParticleSystem feedback;

    [DrawIf("code", codeType.keypart)] public GameObject objToEnable;

    [DrawIf("code", codeType.key)] public Transform hole;
    [DrawIf("code", codeType.key)] public AudioSource assemblement;
    bool doOnce = true;

    [DrawIf("code", codeType.keypad)] public int nb;
    GameObject manager;

    [DrawIf("code", codeType.manager)] public GameObject screen;
    [DrawIf("code", codeType.manager)] public AudioSource good;
    [DrawIf("code", codeType.manager)] public AudioSource mauvais;
    [DrawIf("code", codeType.manager)] public string sequenceJoueur;
    [DrawIf("code", codeType.manager)] public GameObject rouage;
    public List<GameObject> key;
    public Material[] mat;
    string sequence = "123425";
    bool valid = true;

    [DrawIf("code", codeType.bouton)] public AudioSource ouverture;

    // Start is called before the first frame update
    void Start()
    {
        if(code == codeType.keypad)
        {
            manager = GameObject.FindGameObjectWithTag("codeManager");
        }
        if (code == codeType.manager)
        {
            Camera.main.GetComponent<CameraCasts>().FakeStart();
        }
    }
        

    // Update is called once per frame
    void Update()
    {
        if(sequenceJoueur.Length == sequence.Length)
        {
            if (valid)
            {
                Invoke("Validation", 1f);
                valid = false;
            }
        }

        if(code == codeType.key && doOnce && transform.GetChild(0).gameObject.activeInHierarchy && transform.GetChild(1).gameObject.activeInHierarchy)
        {
            doOnce = false;
            assemblement.Play();

            transform.GetChild(0).GetComponent<InteractGlow>().canGlow = true;
            transform.GetChild(0).GetComponent<InteractGlow>().rnd = 0;
            transform.GetChild(1).GetComponent<InteractGlow>().canGlow = true;
            transform.GetChild(1).GetComponent<InteractGlow>().rnd = 0;

            transform.GetChild(2).GetComponent<ParticleSystem>().Play();
        }
    }

    public void Validation()
    {
        if (sequenceJoueur == sequence)
        {
            Debug.Log("Code bon");
            good.Play();
            rouage.GetComponent<RouageChiffre>().canTurn = true;
            rouage.transform.GetChild(1).GetComponent<Animator>().SetBool("open", true);
            rouage.GetComponent<InteractGlow>().canGlow = true;
            StartCoroutine(emissive(0.5f));
            GameObject.FindGameObjectWithTag("grille").GetComponent<SonGrille>().PlaySon();
            Destroy(rouage.transform.GetChild(1).gameObject, 1.5f);
        }
        else
        {
            mauvais.Play();
            Debug.Log("Code faux");
            foreach (GameObject go in key)
            {
                var mats = go.GetComponent<MeshRenderer>().materials;
                mats[1] = mat[0];
                go.GetComponent<MeshRenderer>().materials = mats;
                valid = true;
            }
        }

        sequenceJoueur = "";
    }

    public void ButtonPushed()
    {
        if (pushOnce && !mecanism.GetBool("Open"))
        {
            ouverture.Play();
            feedback.Play();
            mecanism.SetBool("Open", !mecanism.GetBool("Open"));
            //mecanism.transform.parent = null;

            if (keyPartGlow != null)
                keyPartGlow.canGlow = true;
        }
    }

    public void addSequence(int i)
    {
        if (manager.GetComponent<Code>().valid)
        {
            manager.GetComponent<Code>().sequenceJoueur += nb;
            manager.GetComponent<Code>().changeMat(i);
        }

        feedback.Play();
    }

    public void changeMat(int i)
    {
        var mats = key[sequenceJoueur.Length - 1].GetComponent<MeshRenderer>().materials;
        mats[1] = mat[i];
        key[sequenceJoueur.Length - 1].GetComponent<MeshRenderer>().materials = mats;
        Debug.Log("change");
    }

    public IEnumerator emissive(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        screen.GetComponent<Renderer>().materials[1].EnableKeyword("_EMISSION");
    }
}
