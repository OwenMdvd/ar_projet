﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{
    private void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    public void QuitAppli()
    {
        Application.Quit();
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }
}

