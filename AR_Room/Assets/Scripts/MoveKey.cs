﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveKey : MonoBehaviour
{
    bool doOnce = true;

    public float speed = 10;
    public GameObject Target;

    private GameObject cleSon;

    public Vector3 mouseDelta = Vector3.zero;

    private Vector3 lastMousePosition = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        Target = GameObject.FindGameObjectWithTag("key");

            if (Target.transform.GetComponent<Cle>())
            {

                if (!Target.transform.GetComponent<Cle>().tour)
                {
                    Target.transform.GetComponent<Cle>().tour = true;
                }
                if (!Target.transform.GetComponent<Cle>().open)
                {

                    Target.transform.Rotate(75 * Time.deltaTime, 0, 0);
                    SetSonCle();
                    cleSon.SetActive(true);
                }
                else
                {
                    Target.transform.Rotate(0, 0, 0);
                    cleSon.SetActive(false);
                }
            }

        /*
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                //Target.transform.position = hit.point;
                if(hit.transform.tag == "key")
                {
                    if (hit.transform.GetComponent<Cle>())
                    {
                        
                        if(!hit.transform.GetComponent<Cle>().tour)
                        {
                            hit.transform.GetComponent<Cle>().tour = true;
                        }
                        if (!hit.transform.GetComponent<Cle>().open)
                        {
                            Target = hit.transform.gameObject;
                            Target.transform.Rotate(-(mouseDelta.magnitude * speed) * Time.deltaTime, 0, 0);
                            SetSonCle();
                            cleSon.SetActive(true);
                        }
                        else
                        {
                            Target.transform.Rotate(0, 0, 0);
                        }
                    }
                }
            } 
        }

        if (Input.GetMouseButtonUp(0))
        {
            cleSon = GameObject.FindGameObjectWithTag("cleSon");
            if (cleSon != null)
            {
                cleSon.SetActive(false);
            }
        }

        mouseDelta = Input.mousePosition - lastMousePosition;

        lastMousePosition = Input.mousePosition;
        */
    }

    public void SetSonCle()
    {
        if (doOnce)
        {
            doOnce = false;
            cleSon = GameObject.FindGameObjectWithTag("cleSon");
            cleSon.GetComponent<AudioSource>().playOnAwake = true;
            cleSon.SetActive(false);
        }
    }
}
