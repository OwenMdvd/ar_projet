﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class CameraCasts : MonoBehaviour
{
    private AudioSource decouverte;
    private AudioSource frottement;
    public AudioClip[] frottements;
    public List<GameObject> poussieres;
    public GameObject rouage;
    int nbPoussiere;
    bool doOnce = true;
    bool boitePresente;

    public AudioClip[] boutons;
    public AudioSource bouton;

    public GameObject hand;

    public Image uiMask;

    Camera cam;

    float interactTimer;

    private void Start()
    {
        cam = Camera.main;
    }

    public void FakeStart()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("poussiere"))
        {
            poussieres.Add(go);
        }
        nbPoussiere = poussieres.Count;

        boitePresente = true;

        rouage = GameObject.Find("Rouage.G");
    }

    void Update()
    {
        GetComponent<Camera>().fieldOfView -= Input.GetAxis("Mouse ScrollWheel") * 10;
        GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 20, 60);

        /*if (poussieres.Count <= 0 && doOnce && boitePresente)
        {
            doOnce = false;
            decouverte = GameObject.FindGameObjectWithTag("decouverte").GetComponent<AudioSource>();
            rouage.GetComponent<RouageChiffre>().canTurn = true;
            rouage.transform.GetChild(1).GetComponent<Animator>().SetBool("open", true);
            Destroy(rouage.transform.GetChild(1).gameObject, 1.5f);
            decouverte.Play();
            GameObject.FindGameObjectWithTag("grille").GetComponent<SonGrille>().PlaySon();
        }*/

        if(Input.GetKey(KeyCode.LeftArrow))
        {
            var move = transform.root.eulerAngles.y;
            move += 1;
            transform.root.eulerAngles = new Vector3(0, move, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            var move = transform.root.eulerAngles.y;
            move -= 1;
            transform.root.eulerAngles = new Vector3(0, move, 0);
        }

            /*if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "poussiere")
                {
                    frottement = GameObject.FindGameObjectWithTag("frottement").GetComponent<AudioSource>();
                    poussieres.Remove(hit.transform.gameObject);
                    frottement.clip = frottements[Random.Range(0, frottements.Length)];
                    frottement.Play();
                    Destroy(hit.transform.gameObject);
                }
            }
        }*/

        interactTimer -= Time.deltaTime;
        
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && interactTimer < 0)
            { // check hits until a valid one is found
                interactTimer = .5f;

                if (hit.collider.tag == "keypad")
                {
                    bouton = GameObject.FindGameObjectWithTag("boutonSon").GetComponent<AudioSource>();
                    hit.transform.GetComponent<Code>().addSequence(hit.transform.GetComponent<Code>().nb);
                    bouton.clip = boutons[Random.Range(0, boutons.Length)];
                    bouton.Play();

                    var tr = hit.transform;

                    Sequence mySequence = DOTween.Sequence();
                    mySequence.Append(tr.DOMove(tr.position + tr.forward * .01f, .1f).SetEase(Ease.Linear));
                    mySequence.Append(tr.DOMove(tr.position, .2f).SetEase(Ease.OutBounce));
                }
                if (hit.collider.tag == "button")
                {
                    bouton = GameObject.FindGameObjectWithTag("boutonSon").GetComponent<AudioSource>();
                    hit.transform.GetComponent<Code>().ButtonPushed();
                    bouton.clip = boutons[Random.Range(0, boutons.Length)];
                    bouton.Play();

                    var tr = hit.transform;

                    Sequence mySequence = DOTween.Sequence();
                    mySequence.Append(tr.DOMove(tr.position + tr.forward * .01f, .1f).SetEase(Ease.Linear));
                    mySequence.Append(tr.DOMove(tr.position, .2f).SetEase(Ease.OutBounce));
                }
                if (hit.collider.tag == "rouage")
                {
                    if (hit.transform.GetComponent<RouageChiffre>().canTurn)
                    {
                        bouton = GameObject.FindGameObjectWithTag("boutonSon").GetComponent<AudioSource>();
                        //Debug.Log(hit.transform.gameObject);
                        hit.transform.DORotate(hit.transform.eulerAngles + new Vector3(0, 0, 36), .4f);
                        bouton.clip = boutons[Random.Range(0, boutons.Length)];
                        bouton.Play();
                    }
                }
                if (hit.collider.tag == "keyPart")
                {
                    hit.transform.GetComponent<Code>().objToEnable.SetActive(true);
                    hit.transform.gameObject.SetActive(false);
                }
                if (hit.collider.tag == "key" && hit.transform.GetChild(0).gameObject.activeInHierarchy && hit.transform.GetChild(1).gameObject.activeInHierarchy && !hit.transform.GetComponent<Cle>().enabled)
                {
                    StartCoroutine(Cle(hit.transform.gameObject));
                    hit.transform.DOMove(hit.transform.GetComponent<Code>().hole.transform.position, 1);

                    Sequence mySequence = DOTween.Sequence();
                    mySequence.Append(hit.transform.DOMove(hit.transform.GetComponent<Code>().hole.transform.position + new Vector3(0, .1f, 0), 1));
                    mySequence.Join(hit.transform.DORotateQuaternion(hit.transform.GetComponent<Code>().hole.transform.rotation, 1));
                    mySequence.Append(hit.transform.DOMove(hit.transform.GetComponent<Code>().hole.transform.position, 1));

                    transform.GetComponent<MoveKey>().enabled = true;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    { 
        uiMask = GameObject.FindGameObjectWithTag("hide").GetComponent<Image>();
        var c = uiMask.color;
        if (other.tag == "HideCam")
        {
            uiMask.color = new Color(c.r, c.g, c.b, 1);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var c = uiMask.color;
        if (other.tag == "HideCam")
        {
            uiMask.color = new Color(c.r, c.g, c.b, 0);
        }
    }

    IEnumerator Cle(GameObject go)
    {
        yield return new WaitForSeconds(2);
        go.GetComponent<Cle>().enabled = true;
        //Destroy(go);
        //GameObject.FindGameObjectWithTag("emplacement").transform.GetChild(0).gameObject.SetActive(true);
    }
}
