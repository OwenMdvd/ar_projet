﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poussiere : MonoBehaviour
{
    public Sprite[] text;

    // Start is called before the first frame update
    void Start()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, Random.Range(0, 360));
        GetComponent<SpriteRenderer>().sprite = text[Random.Range(0, text.Length)];
    }

}
