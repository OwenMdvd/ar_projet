﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stratch : MonoBehaviour
{
    //CopiedTexture is the original Texture  which you want to copy.
    public Texture2D CopyTexture2D(Texture2D copiedTexture, int i, int j)
    {
        //Create a new Texture2D, which will be the copy.
        Texture2D texture = new Texture2D(copiedTexture.width, copiedTexture.height);
        //Choose your filtermode and wrapmode here.
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;

        int y = 0;
        while (y < texture.height)
        {
            int x = 0;
            while (x < texture.width)
            {
                //INSERT YOUR LOGIC HERE
                    //This line of code and if statement, turn Green pixels into Red pixels.
                    if (y==i && x == j)
                    {
                        Color newColor = new Color(0,0,0,0);
                        texture.SetPixel(x, y, newColor);
                        Debug.Log("Noir");
                    }
                else
                {
                    //This line of code is REQUIRED. Do NOT delete it. This is what copies the image as it was, without any change.
                    texture.SetPixel(x, y, copiedTexture.GetPixel(x, y));
                }
                ++x;
            }
            ++y;
        }
        //Name the texture, if you want.
        texture.name = ("_SpriteSheet");

        //This finalizes it. If you want to edit it still, do it before you finish with .Apply(). Do NOT expect to edit the image after you have applied. It did NOT work for me to edit it after this function.
        texture.Apply();

        //Return the variable, so you have it to assign to a permanent variable and so you can use it.
        return texture;
    }

    public void UpdateCharacterTexture(Vector2 coord)
    {
        //This calls the copy texture function, and copies it. The variable characterTextures2D is a Texture2D which is now the returned newly copied Texture2D.
        Texture2D characterTexture2D = CopyTexture2D(gameObject.GetComponent<SpriteRenderer>().sprite.texture, Mathf.FloorToInt(coord.x) , Mathf.FloorToInt(coord.y));

        //Get your SpriteRenderer, get the name of the old sprite,  create a new sprite, name the sprite the old name, and then update the material. If you have multiple sprites, you will want to do this in a loop- which I will post later in another post.
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        string tempName = sr.sprite.name;
        sr.sprite = Sprite.Create(characterTexture2D, sr.sprite.rect, new Vector2(0, 1));
        sr.sprite.name = tempName;

        Debug.Log(sr.sprite.name);

    }

    private void Start()
    {
    }

    public Camera cam;


    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 10000))
            {
                Debug.Log("Appuye");

                Vector2 pixelUV = hit.textureCoord;
                UpdateCharacterTexture(pixelUV);
            }
        }
    }
}
