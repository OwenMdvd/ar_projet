﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Video;
using System.Collections;
using System.Collections.Generic;

public class LoadVideo : MonoBehaviour
{
    public GameObject musique;
     RawImage rawImage;
     VideoPlayer videoPlayer;

    // Start is called before the first frame update
    void Start()
    {
        rawImage = GetComponent<RawImage>();
        videoPlayer = GetComponent<VideoPlayer>();
        StartCoroutine(PlayVideo());
    }

    IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        WaitForSeconds wait = new WaitForSeconds(1);
        while(!videoPlayer.isPrepared)
        {
            yield return wait;
            break;
        }
        rawImage.color = new Color(1, 1, 1, 1);
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
        musique.SetActive(true);
    }
}
