﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSprite : MonoBehaviour
{
    Texture2D textureLoad;
    public SpriteRenderer Oui;

    // Start is called before the first frame update
    void Start()
    {
        textureLoad = CopyTex(Oui.sprite.texture);
        Sprite sprite = Sprite.Create(textureLoad, new Rect(0, 0, textureLoad.width, textureLoad.height), new Vector2(0.5f, 0.5f));
        Oui.sprite = sprite;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 pos = Input.mousePosition;
            Camera _cam = Camera.main;
            Ray ray = _cam.ScreenPointToRay(pos);
            RaycastHit hit;
            if (Physics.Raycast(_cam.transform.position, ray.direction, out hit, 10000.0f))
            {
                Color c;
                if (hit.collider)
                {
                    Texture2D tex = (Texture2D)hit.collider.gameObject.GetComponent<Renderer>().material.mainTexture; // Get texture of object under mouse pointer
                    c = tex.GetPixelBilinear(hit.textureCoord2.x, hit.textureCoord2.y); // Get color from texture
                    Debug.Log(c);
                }
            }
        }
    }

    public Texture2D CopyTex(Texture2D copiedTexture)
    {
        // Create a new Texture2D, which will be the copy.
         Texture2D texture = new Texture2D(copiedTexture.width, copiedTexture.height);
        //Choose your filtermode and wrapmode here.
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;

        int y = 0;
        while (y < texture.height)
        {
            int x = 0;
            while (x < texture.width)
            {
                if (x == 20)
                {
                    Color newColor = new Color(0, 0, 0, 0);
                    texture.SetPixel(x, y, newColor);
                }
                else
                {
                    //This line of code is REQUIRED. Do NOT delete it. This is what copies the image as it was, without any change.
                    texture.SetPixel(x, y, copiedTexture.GetPixel(x, y));
                }
                ++x;
            }
            ++y;
        }

        //This finalizes it. If you want to edit it still, do it before you finish with .Apply(). Do NOT expect to edit the image after you have applied. It did NOT work for me to edit it after this function.
        texture.Apply();

        //Return the variable, so you have it to assign to a permanent variable and so you can use it.
        return texture;
    }
}
