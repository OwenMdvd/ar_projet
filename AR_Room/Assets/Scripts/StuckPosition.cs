﻿using UnityEngine.UI;
using UnityEngine;

public class StuckPosition : MonoBehaviour
{
    private Vector3 pos;
    private Quaternion rota;

    // Start is called before the first frame update
    void Awake()
    {
        pos = transform.position;
        rota = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = pos;
        transform.rotation = rota;

        GameObject.FindGameObjectWithTag("textPos").GetComponent<Text>().text = pos.ToString();
    }
}
