﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonGrille : MonoBehaviour
{
    public AudioClip[] sonGrille;

    public void PlaySon()
    {
        GetComponent<AudioSource>().clip = sonGrille[Random.Range(0, sonGrille.Length)];
        GetComponent<AudioSource>().Play();
    }
}
