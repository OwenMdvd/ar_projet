﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAllOutline : MonoBehaviour
{
    public void EnableAllOutlines()
    {
        //GameObject.FindGameObjectWithTag("viewTrough").SetActive(true);
        Camera.main.transform.GetChild(0).gameObject.SetActive(true);

        foreach (InteractGlow ig in FindObjectsOfType<InteractGlow>())
        {
            ig.EnableOutline();
        }

    }
}
