﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehavior : MonoBehaviour
{
    public bool isGrabbed;
    
    void Start()
    {
        isGrabbed = false;
    }
    
    void Update()
    {
        Material myMat = GetComponent<Renderer>().material;
        if (isGrabbed) 
            myMat.SetColor("_EmissionColor", new Vector4(1, 1, 1, 0));
        else
            myMat.SetColor("_EmissionColor", new Vector4(0, 0, 0, -10));

        //isGrabbed = false;
        //if (Input.GetMouseButton(0))
        //{
            /*RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, layers))
            {
                Transform objectHit = hit.transform;

                if (hit.transform.gameObject == gameObject)
                {
                    isGrabbed = true;
                }
            }*/

            /*Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            var hits = Physics.RaycastAll(ray, 1000); // get all hits
            foreach (RaycastHit hit in hits)
            { // check hits until a valid one is found
                if (hit.collider.gameObject == gameObject)
                {
                    isGrabbed = true;
                    hand.GetComponent<SpringJoint>().connectedBody = gameObject.GetComponent<Rigidbody>();
                }
            }
        }
        else
        {
            isGrabbed = false;
            hand.GetComponent<SpringJoint>().connectedBody = null;
        }*/


    }

    void Action()
    {

    }

    /*private void OnMouseDown()
    {
        isGrabbed = true;
    }

    private void OnMouseUpAsButton()
    {
        isGrabbed = false;
        Action();
    }

    private void OnMouseExit()
    {
        isGrabbed = false;
    }*/
}
