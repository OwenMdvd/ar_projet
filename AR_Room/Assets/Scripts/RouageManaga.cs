﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouageManaga : MonoBehaviour
{
    public int[] codeNeeded;
    public int[] code;

    public Animator crateOpen;
    public GameObject phone;
    public AudioSource ouverture;

    public ParticleSystem[] psDust;

    void Start()
    {
        
    }
    
    void Update()
    {
        code[0] = transform.GetChild(0).GetComponent<RouageChiffre>().nb();
        code[1] = transform.GetChild(1).GetComponent<RouageChiffre>().nb();
        code[2] = transform.GetChild(2).GetComponent<RouageChiffre>().nb();

        if (codeNeeded[0] == code[0] && codeNeeded[1] == code[1] && codeNeeded[2] == code[2] && !crateOpen.GetBool("Open"))
        {
            Debug.Log("YOOOOOUUUUUUUUU WIINNNNNNNNAAAAAAAAAA !!");
            crateOpen.SetBool("Open", true);
            phone.SetActive(true);
            ouverture.Play();
            GameObject.FindGameObjectWithTag("end").GetComponent<Animator>().SetTrigger("Win");

            foreach (ParticleSystem dust in psDust)
                dust.Play();
        }
    }
}
