﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class InteractGlow : MonoBehaviour
{
    public bool canGlow;
    public float rnd;

    public Material[] mats;
    public Material outline;

    public bool disableGlow = true;

    private void Start()
    {
        rnd = Random.value;

        if (disableGlow)
            StartCoroutine(DisableGlow());
    }

    IEnumerator DisableGlow()
    {
        yield return new WaitForEndOfFrame();

        outline = GetComponent<Renderer>().materials[2] = outline;

        mats = new Material[3];
        mats[0] = GetComponent<Renderer>().materials[0];
        mats[1] = GetComponent<Renderer>().materials[1];
        mats[2] = GetComponent<Renderer>().materials[1];

        GetComponent<Renderer>().materials = mats;
    }

    void Update()
    {/*
        if (canGlow)
        {
            Color newColor = GetComponent<Renderer>().materials[1].color;
            newColor.a = Mathf.Clamp((Mathf.Cos(Time.time / 1 + rnd * 10) - .8f) * 4 , 0, 1);
            GetComponent<Renderer>().materials[1].color = newColor;
        }
        else
        {
            Color newColor = GetComponent<Renderer>().materials[1].color;
            newColor.a = 0;
            GetComponent<Renderer>().materials[1].color = newColor;
        }*/

        if (Input.GetKeyDown(KeyCode.A))
            EnableOutline();

        if (Input.GetKeyDown(KeyCode.Z))
            DisableOutline();
    }

    public void EnableOutline()
    {
        if (mats.Length > 2)
        {
            mats[2] = outline;
            GetComponent<Renderer>().materials = mats;
        }
    }

    public void DisableOutline()
    {
        mats[2] = GetComponent<Renderer>().materials[1];
        GetComponent<Renderer>().materials = mats;
    }
}