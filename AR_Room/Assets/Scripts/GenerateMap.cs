﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateMap : MonoBehaviour
{

    int Max;

    [Tooltip("Nombre de Tile sur la map +1")]
    public int NbTile;

    public GameObject Map;

    public Texture text;

    public GameObject test;

    // Use this for initialization
    void Start()
    {

        CopyTex(Map.GetComponent<SpriteRenderer>().sprite.texture);

        Max = (NbTile * 10) / 2;

        var tex = CopyColor(Map.GetComponent<SpriteRenderer>().sprite.texture, 100, 100);
        Sprite sprite = Sprite.Create(tex, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f));
        test.GetComponent<SpriteRenderer>().sprite = sprite;

        for (int j = -Max; j <= Max; j += 1)
        {
            for (int i = Max; i >= -Max; i -= 1)
            {
                GameObject Test = Instantiate(Map, new Vector3(transform.position.x + i/100, transform.position.y, transform.position.z + j/100), Quaternion.Euler(new Vector3(90, 0, 0)));
                var textureLoad = CopyColor(Map.GetComponent<SpriteRenderer>().sprite.texture, i, j);
                Sprite spritetoLoad = Sprite.Create(textureLoad, new Rect(0, 0, textureLoad.width, textureLoad.height), new Vector2(0.5f, 0.5f));
                Test.GetComponent<SpriteRenderer>().sprite = spritetoLoad;
                Test.transform.parent = gameObject.transform;
            }
        }

    }

    public Texture2D CopyTex(Texture2D copiedTexture)
    {
        // Create a new Texture2D, which will be the copy.
        Texture2D texture = new Texture2D(copiedTexture.width, copiedTexture.height);
        //Choose your filtermode and wrapmode here.
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;

        int y = 0;
        while (y < texture.height)
        {
            int x = 0;
            while (x < texture.width)
            {
                if (x == 20)
                {
                    Color newColor = new Color(0, 0, 0, 0);
                    texture.SetPixel(x, y, newColor);
                }
                else
                {
                    //This line of code is REQUIRED. Do NOT delete it. This is what copies the image as it was, without any change.
                    texture.SetPixel(x, y, copiedTexture.GetPixel(x, y));
                }
                ++x;
            }
            ++y;
        }

        //This finalizes it. If you want to edit it still, do it before you finish with .Apply(). Do NOT expect to edit the image after you have applied. It did NOT work for me to edit it after this function.
        texture.Apply();

        //Return the variable, so you have it to assign to a permanent variable and so you can use it.
        return texture;
    }

    public Texture2D CopyColor(Texture2D copiedTexture, int j, int i)
    {
        // Create a new Texture2D, which will be the copy.
        Texture2D texture = new Texture2D(1, 1);
        //Choose your filtermode and wrapmode here.
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;

        int y = 0;
        while (y < texture.height)
        {
            int x = 0;
            while (x < texture.width)
            {
                texture.SetPixel(x, y, copiedTexture.GetPixel(i, j));

                ++x;
            }
            ++y;
        }

        //This finalizes it. If you want to edit it still, do it before you finish with .Apply(). Do NOT expect to edit the image after you have applied. It did NOT work for me to edit it after this function.
        texture.Apply();

        //Return the variable, so you have it to assign to a permanent variable and so you can use it.
        return texture;
    }
}
